import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';

import { MaterialModule } from './modules/material.module';

import { AppComponent } from './app.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { RestaurantListComponent } from './components/restaurant-list/restaurant-list.component';

import { hummusRestaurantsReducer } from './store/hummus.reducer';
import { RestaurantItemComponent } from './components/restaurant-item/restaurant-item.component';

@NgModule({
  declarations: [AppComponent, SearchbarComponent, RestaurantListComponent, RestaurantItemComponent],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    MaterialModule,
    StoreModule.forRoot({ hummusList: hummusRestaurantsReducer })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
