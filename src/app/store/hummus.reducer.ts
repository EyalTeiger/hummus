import * as HummusActions from './hummus.actions';
import { HummusPlaces } from '../shared/mock-data';
import { HummusPlace } from '../shared/models';

const initialState = {
  activeRestaurants: [],
  pastSearches: []
};

export function hummusRestaurantsReducer(
  state = initialState,
  action: HummusActions.HummusActions
) {
  switch (action.type) {
    case HummusActions.SEARCH_HUMMUS_RESTAURANT:
      const filtered: HummusPlace[] = HummusPlaces.filter(place => {
        return place.name.toLowerCase().includes(action.payload);
      });

      return {
        ...state,
        activeRestaurants: [...filtered],
        pastSearches: [
          ...state.pastSearches,
          { value: action.payload, date: Date.now() }
        ]
      };
  }
  return state;
}
