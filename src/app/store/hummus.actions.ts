import { Action } from '@ngrx/store';

export const SEARCH_HUMMUS_RESTAURANT = 'SEARCH_HUMMUS_RESTAURANT';

export class SearchHummusRestaurant implements Action {
  readonly type = SEARCH_HUMMUS_RESTAURANT;

  constructor(public payload: string) {}
}

export type HummusActions = SearchHummusRestaurant;
