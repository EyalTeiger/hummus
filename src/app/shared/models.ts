export interface HummusPlace {
  name: string;
  description: string;
  location: string;
  open: boolean;
  link?: string;
}

export interface PastSearch {
  value: string;
  date: Date;
}

export interface HummusState {
  activeRestaurants: HummusPlace[];
  pastSearches: PastSearch[];
}
