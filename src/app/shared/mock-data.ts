import { HummusPlace } from './models';

export const HummusPlaces: HummusPlace[] = [
  {
    name: 'Abu Hassan',
    description: 'Great Place',
    location: 'Hadolfin st., Jaffa',
    open: false
  },
  {
    name: 'Hasurim',
    description: 'Great Place',
    location: 'Malan 28, Tel-Aviv',
    open: false
  },
  {
    name: 'Abu Said',
    description: 'Great Place',
    location: 'Akko market, Akko',
    open: false
  },
  {
    name: 'Issa',
    description: 'Great Place',
    location: 'Sallah Al-Din, Akko',
    open: false
  },
  {
    name: 'Bahadunas',
    description: 'Great Place',
    location: 'Bialik 138, Ramat-Gan',
    open: false
  },
  {
    name: 'Ikramahui',
    description: 'Great Place',
    location: 'Haneviim 2, Jerusalem',
    open: false
  },
  {
    name: 'Farage',
    description: 'Great Place',
    location: 'Hameginim 29, Haifa',
    open: false
  },
  {
    name: 'Halil',
    description: 'Great Place',
    location: 'Kehilat Detroit 6, Ramla',
    open: false
  },
  {
    name: 'Gingi',
    description: 'Great Place',
    location: 'Kibbutz Einat',
    open: false
  }
];
