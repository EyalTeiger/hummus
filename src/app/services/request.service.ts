import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { HummusState } from '../shared/models';

import * as HummusActions from '../store/hummus.actions';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  constructor(
    private store: Store<{
      hummusRestaurantsReducer: HummusState;
    }>
  ) {}

  public findPlaces(input: string) {
    this.store.dispatch(new HummusActions.SearchHummusRestaurant(input));
  }
}
