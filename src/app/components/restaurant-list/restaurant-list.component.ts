import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { HummusPlace, HummusState } from 'src/app/shared/models';
import { Observable } from 'rxjs';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.scss']
})
export class RestaurantListComponent implements OnInit {
  activeRestaurantsState: Observable<HummusState>;
  test;
  isValid = true;

  constructor(
    private store: Store<{
      hummusRestaurantsReducer: {
        hummusList: { activeRestaurants: HummusPlace[] };
      };
    }>,
    private requestService: RequestService
  ) {}

  ngOnInit() {
    this.activeRestaurantsState = this.store.select('hummusList');
  }

  public searchHummus(pastSearch) {
    this.requestService.findPlaces(pastSearch);
  }

  get pastSearches() {
    let past;

    this.activeRestaurantsState.subscribe(state => {
      past = state.pastSearches;
    });

    return past;
  }
}
