import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {
  public form: FormGroup;

  constructor(private fb: FormBuilder, private requestService: RequestService) {
    this.form = this.fb.group({
      restaurantFilter: ['']
    });
  }

  ngOnInit() {}

  get hummusFilter(): string {
    return this.form.controls.restaurantFilter.value;
  }

  public searchHummus(userInput: string) {
    this.requestService.findPlaces(userInput.toLowerCase());
    this.form.reset();
  }
}
